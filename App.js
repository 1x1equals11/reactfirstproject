import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicator from './screens/ActivityIndicator'
import DrawerLayout from './screens/DrawerLayout'
import List from './screens/FlatList'
import DisplayAnImage from './screens/Image'
import KeyBoard from './screens/KeyBoard'
import MyComponent from './screens/ListView'
import ModalExample from './screens/Modals'
import PickerExample from './screens/Picker'
import ProgressBar from './screens/Progress'
import RefreshableList from './screens/refreshcontrol'
import SafeArea from './screens/SafeAreaView'
import ScrollExample from './screens/scrollview'
import SectionListExample from './screens/sectionlist'
import SliderExample from './screens/slider'
import StatusBarExample from './screens/statusbar'
const RootStack = createStackNavigator({
    Home: {
      screen: HomeScreen
    },
    Details: {
      screen: DetailsScreen
    },
    ActivityIndicator:{
      screen: ActivityIndicator
    },
    Drawer: {
      screen: DrawerLayout
    },
    FList: {
      screen: List
    },
    DisplayImage: {
      screen: DisplayAnImage
    },
    KeyboardAvoid: {
      screen: KeyBoard
    },
    LView: {
      screen: MyComponent
    },
    Modall: {
      screen: ModalExample
    },
    Pick: {
      screen: PickerExample
    },
    Progress: {
      screen: ProgressBar
    },
    Refresh: {
      screen: RefreshableList
    },
    Safe:{
      screen: SafeArea
    },
    Scroll:{
      screen: ScrollExample
    },
    Section:{
      screen: SectionListExample
    },
    Slide:{
      screen: SliderExample
    },
    Status:{
      screen: StatusBarExample
    }
  },
);


export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
