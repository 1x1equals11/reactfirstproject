import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class DetailsScreen extends Component {
    render() {
      /* 2. Read the params from the navigation state */
     const { params } = this.props.navigation.state;
     const itemId = params ? params.itemId : null;
     const otherParam = params ? params.otherParam : null;

        return (
          <View style={styles.container}>
            <Text>Details Screen</Text>
            <Text>itemId: {JSON.stringify(itemId)}</Text>
            <Text>otherParam: {JSON.stringify(otherParam)}</Text>
            <Button
              title="Go to Details... again"
              onPress={() => this.props.navigation.navigate('Details')}
            />
            <Button
              title="Go back"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });


DetailsScreen.propTypes = {

};

export default DetailsScreen;
