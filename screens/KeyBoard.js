import React, {Component} from 'react';
import { KeyboardAvoidingView,Text, TextInput, StyleSheet } from 'react-native';

class KeyBoard extends Component{
  render(){
    return(
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <TextInput
          style={{height: 40, width:250}}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({text})}
        />
        <Text>Test ra nii~{"\n\n\n\n\n"}</Text>
        <Text>Test ra nii baaaaaa{"\n\n\n\n\n"}</Text>
        <Text>Test ra nii syaaaaaa baaaaa{"\n\n\n"}</Text>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

export default KeyBoard;
