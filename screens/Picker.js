import React, {Component} from 'react';
import {Picker} from 'react-native';

class PickerExample extends Component{
  constructor(props){
    super(props);
    this.state = {
      language: 'Default'
    }
  }

  render() {
    return(
      <Picker
        selectedValue={this.state.language}
        style={{ height: 50, width: 100 }}
        onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
      </Picker>
    );
  }
}

export default PickerExample;
