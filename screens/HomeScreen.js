import React, {Component} from 'react';
import { StyleSheet, Text, View, Button,ScrollView } from 'react-native';

class HomeScreen extends Component {
//C:\Users\Claude\Desktop\GitFolder\AwesomeProject
    render() {
        return (
          <ScrollView style={styles.container}>
            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details', {
              itemId: 86,
              otherParam: 'anything you want here',
            })}
            />
            <Button
              title="ActivityIndicator"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />
            <Button
              title="DrawerLayout"
              onPress={() => this.props.navigation.navigate('Drawer')}
            />
            <Button
              title="FlatList"
              onPress={() => this.props.navigation.navigate('FList')}
            />
            <Button
              title="Images"
              onPress={() => this.props.navigation.navigate('DisplayImage')}
            />
            <Button
              title="KeyboardAvoidingView"
              onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
            />
            <Button
              title="ListView"
              onPress={() => this.props.navigation.navigate('LView')}
            />
            <Button
              title="Modal"
              onPress={() => this.props.navigation.navigate('Modall')}
            />
            <Button
              title="Picker"
              onPress={() => this.props.navigation.navigate('Pick')}
            />
            <Button
              title="ProgressBar"
              onPress={() => this.props.navigation.navigate('Progress')}
            />
            <Button
              title="Refresh Control"
              onPress={() => this.props.navigation.navigate('Refresh')}
            />
            <Button
              title="Safe Area View"
              onPress={() => this.props.navigation.navigate('Safe')}
            />
            <Button
              title="Scroll View"
              onPress={() => this.props.navigation.navigate('Scroll')}
            />
            <Button
              title="SectionList"
              onPress={() => this.props.navigation.navigate('Section')}
            />
            <Button
              title="Slider"
              onPress={() => this.props.navigation.navigate('Slide')}
            />
            <Button
              title="Status Bar"
              onPress={() => this.props.navigation.navigate('Status')}
            />
          </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });


HomeScreen.propTypes = {

};

export default HomeScreen;
