import React, { Component } from 'react';
import {FlatList,Text} from 'react-native';

class List extends Component {
    render() {
        return (
            <FlatList
            data={[
                {key: 'James Claude Lequin'},
                {key: 'Kimberly Devocion'},
            ]}
            renderItem={({item}) => <Text>{item.key}</Text>}
            />
        );
    }
}

export default List;
