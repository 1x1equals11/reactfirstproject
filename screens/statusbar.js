import React,{Component}from'react';
import {View,StatusBar}from'react-native';

class StatusBarExample extends Component{
  render(){
    return(
      <View>
      <StatusBar
        backgroundColor="blue"
        barStyle = "dark-content" 
      />
    </View>
    );
  }
}

export default StatusBarExample;
