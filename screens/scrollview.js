import React, {Component} from 'react';
import {ScrollView,Text,View} from 'react-native';

class ScrollExample extends Component{
  render(){
    return(
       //<ScrollView>
       <ScrollView horizontal={true}>
         <Text style={{fontSize:40,padding:15, backgroundColor:"green"}}>
         I AM GREEN
         </Text>

         <Text style={{fontSize:40,padding:45, backgroundColor:"pink"}}>
         I AM PINK
         </Text>

         <Text style={{fontSize:40,padding:45, backgroundColor:"yellow"}}>
         I AM YELLOW
         </Text>

         <Text style={{fontSize:40,padding:45, backgroundColor:"red"}}>
         I AM RED
         </Text>
        </ScrollView>
       //</ScrollView>
    );
  }
}

export default ScrollExample;
